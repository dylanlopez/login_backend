﻿using Domain.Dtos;

namespace Domain.Queries
{
    public interface IUserQuery
    {
        User CreateUser(User user);
        User LoginUser(User user);
    }
}
