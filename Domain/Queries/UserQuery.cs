﻿using Domain.Dtos;
using Domain.Security;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Domain.Queries
{
    public class UserQuery : IUserQuery
    {
        private ILogger _logger;
        private SqliteCommand _command;
        private string _dbPath;
        private string _keyString;

        public UserQuery(ILogger logger, string connectionString, string key)
        {
            _logger = logger;
            _dbPath = Directory.GetCurrentDirectory() + "\\" + connectionString;
            _keyString = key;
            ValidateTable();
        }

        private void ValidateTable()
        {
            var create = @"CREATE TABLE IF NOT EXISTS 
                            User(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                 Email TEXT NOT NULL, 
                                 Password TEXT NOT NULL, 
                                 Active INTEGER NOT NULL)";
            try
            {
                using (SqliteConnection connection = new SqliteConnection($"Filename={_dbPath}"))
                {
                    connection.Open();
                    _command = new SqliteCommand(create, connection);
                    _command.ExecuteNonQuery();
                    _logger.LogInformation("Table User created");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Table User already exists");
            }
        }

        public User CreateUser(User user)
        {
            User result = null;
            var insert = @"INSERT INTO User(Email, Password, Active) VALUES (@email, @password, 1); SELECT last_insert_rowid()";
            try
            {
                using (SqliteConnection connection = new SqliteConnection($"Filename={_dbPath}"))
                {
                    connection.Open();
                    var passwordE = Security.Security.Encrypt(user.PasswordD, _keyString);
                    using (var transaction = connection.BeginTransaction())
                    {
                        result = new User();
                        _command = connection.CreateCommand();
                        _command.CommandText = insert;
                        _command.Parameters.AddWithValue("@email", user.Email);
                        _command.Parameters.AddWithValue("@password", passwordE);
                        result.Id = Convert.ToInt32(_command.ExecuteScalar());
                        transaction.Commit();
                        result.Email = user.Email;
                        result.Password = passwordE;
                        result.PasswordD = user.PasswordD;
                        result.Active = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("LoginUser Error: " + ex.Message);
            }
            return result;
        }
        public User LoginUser(User user)
        {
            User result = null;
            var select = @"SELECT * FROM User WHERE Email LIKE @email AND Password LIKE @password AND Active = 1";
            try
            {
                using (SqliteConnection connection = new SqliteConnection($"Filename={_dbPath}"))
                {
                    connection.Open();
                    var passwordE = Security.Security.Encrypt(user.PasswordD, _keyString);
                    _command = new SqliteCommand(select, connection);
                    _command.Parameters.AddWithValue("@email", user.Email);
                    _command.Parameters.AddWithValue("@password", passwordE);
                    var reader = _command.ExecuteReader();
                    while (reader.Read())
                    {
                        result = new User();
                        result.Id = reader.GetInt32(0);
                        result.Email = reader.GetString(1);
                        result.Password = reader.GetString(2);
                        result.PasswordD = Security.Security.Decrypt(result.Password, _keyString);
                        result.Active = reader.GetInt32(3);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("LoginUser Error: " + ex.Message);
            }
            return result;
        }
    }
}
