﻿using API.Models;
using Domain.Dtos;
using Domain.Queries;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [EnableCors("AllowAny")]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private IUserQuery query;

        public LoginController(ILogger<LoginController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpPost("createUser")]
        public ResponseBase CreateUser([FromBody] UserRequest request)
        {
            ResponseBase response = new ResponseBase();
            var dbPath = _configuration["Database"];
            var keyString = _configuration["Key"];
            query = new UserQuery(_logger, dbPath, keyString);
            User user = new User();
            user.Email = request.Email;
            user.PasswordD = request.Password;
            var resp = query.CreateUser(user);
            response.Data.Add(resp);
            return response;
        }

        [HttpPost("loginUser")]
        public ResponseBase LoginUser([FromBody] UserRequest request)
        {
            ResponseBase response = new ResponseBase();
            var dbPath = _configuration["Database"];
            var keyString = _configuration["Key"];
            query = new UserQuery(_logger, dbPath, keyString);
            User user = new User();
            user.Email = request.Email;
            user.PasswordD = request.Password;
            var resp = query.LoginUser(user);
            response.Data.Add(resp);
            return response;
        }
    }
}
