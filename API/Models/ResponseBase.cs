﻿using Domain.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.Models
{
    public class ResponseBase
    {
        public ResponseBase()
        {
            Code = 200;
            Message = "Succeful";
            Data = new List<User>();
        }

        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "data")]
        public List<User> Data { get; set; }
    }
}
