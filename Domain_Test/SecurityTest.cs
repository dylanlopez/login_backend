using Domain.Security;
using System;
using Xunit;

namespace Domain_Test
{
    public class SecurityTest
    {
        public static string _expectedOK = "fEAjwqwvKCk9Wy4tNzM0XRK/TkETqXqZPS1DuN1mXgo60bwXmmgtiKlHtbF2aYjV";

        [Fact]
        public void EncryptOk()
        {
            var textToEncrypt = "Test of encryption";
            var keyString = "TEST20211202TSET";
            var encrypt = Security.Encrypt(textToEncrypt, keyString);
            Assert.Equal(_expectedOK, encrypt);
        }

        [Fact]
        public void EncryptErrorLength()
        {
            var textToEncrypt = "Test of encryption";
            var keyString = "TEST2021";
            Assert.Throws<ArgumentException>(() => Security.Encrypt(textToEncrypt, keyString));
        }
    }
}
